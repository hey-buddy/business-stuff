from django.apps import AppConfig


class PatronagemConfig(AppConfig):
    name = 'patronagem'
