from .base import *


SECRET_KEY = get_env_variable('PROD_KEY')

ALLOWED_HOSTS += ('0.0.0.0')
